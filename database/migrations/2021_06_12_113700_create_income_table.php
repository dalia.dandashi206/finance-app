<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
             Schema::create('profits', function (Blueprint $table) {
            $table->increments('profit_id')->primaryKey;
            $table->double('profit_goal');
            $table->timestamps();
        }); 

         
            Schema::create('admins', function (Blueprint $table) {
            $table->increments('admin_id')->primaryKey;
            $table->string('username');
            $table->string('password');
            $table->string('profile_picture');
            $table->timestamps();
        });
         


Schema::create('categories', function (Blueprint $table) {
            $table->increments('categories_id')->primaryKey;
            $table->string('categories_name');
            $table->integer('admin_id')->unsigned();
            $table->timestamps();
           $table->foreign('admin_id')
                  ->references('admin_id')
                  ->on('admins')
                  ->onDelete('cascade');
        });
  
        Schema::create('incomes', function (Blueprint $table) {
            $table->increments('income_id')->primaryKey;
            $table->string('title');
            $table->string('description');
            $table->string('currency');
            $table->double('amount');
            $table->string('recurring')->nullable();
            $table->timestamp('start_date')->useCurrent();
            $table->timestamp('end_date')->nullable();
            $table->string('type');
            $table->integer('categories_id')->unsigned();
            $table->timestamps();
           $table->foreign('categories_id')
                  ->references('categories_id')
                  ->on('categories')
                  ->onDelete('cascade');
        });
         Schema::create('expenses', function (Blueprint $table) {
            $table->increments('expenses_id')->primaryKey;
            $table->string('title');
            $table->string('description');
            $table->string('currency');
            $table->double('amount');
            $table->string('recurring')->nullable();
            $table->timestamp('start_date')->useCurrent();
            $table->timestamp('end_date')->nullable();
            $table->timestamps();
             $table->string('type');
            $table->integer('categories_id')->unsigned();
           $table->foreign('categories_id')
                  ->references('categories_id')
                  ->on('categories')
                  ->onDelete('cascade');
        });

       Schema::create('income_sub', function (Blueprint $table) {
            $table->increments('income_sub_id')->primaryKey;
            $table->string('title');
            $table->string('currency');
            $table->double('amount');
            $table->timestamp('start_date')->useCurrent();
            $table->timestamp('end_date')->nullable();
            $table->string('status');
            $table->integer('income_id')->unsigned();
            $table->timestamps();
           $table->foreign('income_id')
                  ->references('income_id')
                  ->on('incomes')
                  ->onDelete('cascade');
        });
         Schema::create('expenses_sub', function (Blueprint $table) {
            $table->increments('expenses_sub_id')->primaryKey;
            $table->string('title');
            $table->string('currency');
            $table->double('amount');
            $table->timestamp('start_date')->useCurrent();
            $table->timestamp('end_date')->nullable();
            $table->string('status');
            $table->integer('expenses_id')->unsigned();
            $table->timestamps();
           $table->foreign('expenses_id')
                  ->references('expenses_id')
                  ->on('expenses')
                  ->onDelete('cascade');
        });
  
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profits');
        Schema::dropIfExists('admins');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('incomes');
        Schema::dropIfExists('expenses');
        Schema::dropIfExists('income_sub');
        Schema::dropIfExists('expenses_sub');
    }
}
