<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Income;

class IncomeController extends Controller
{
    public function storeincome(Request $request){
      //  dd($request);
      $income = new Income();
      $income->fill($request->all());
      $income->save();
      dd($income);

    }
    public function getallincomes(){
        return Income::all();
    }

        public function updateincome(Request $request,$income_id){
            $admin = Income::where('income_id',$income_id)->first();
            //return $blog;
            $admin->update($request->all());
            $admin->save();
    }
      public function deleteincome($income_id){
          Income::where('income_id',$income_id)->delete();
    }
}
