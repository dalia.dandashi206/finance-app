<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profit;

class ProfitController extends Controller
{
    public function storeprofit(Request $request){
      //  dd($request);
      $profit = new Profit();
      $profit->fill($request->all());
      $profit->save();
      dd($profit);

    }
    public function getallprofits(){
        return Profit::all();
    }

        public function updateprofit(Request $request,$profit_id){
            $blog = Profit::where('profit_id',$profit_id)->first();
            //return $blog;
            $blog->update($request->all());
            $blog->save();
    }
      public function deleteprofit($profit_id){
          Profit::where('profit_id',$profit_id)->delete();
    }
}
