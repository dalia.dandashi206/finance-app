<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Expenses;


class ExpensesController extends Controller
{
     public function storeexpense(Request $request){
      //  dd($request);
      $income = new Expenses();
      $income->fill($request->all());
      $income->save();
      dd($income);

    }
    public function getallexpenses(){
        return Expenses::all();
    }

        public function updateexpense(Request $request,$expenses_id){
            $admin = Expenses::where('expenses_id',$expenses_id)->first();
            //return $blog;
            $admin->update($request->all());
            $admin->save();
    }
      public function deleteexpense($expenses_id){
          Expenses::where('expenses_id',$expenses_id)->delete();
    }
}
